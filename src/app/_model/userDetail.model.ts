export class UserDetail {
    connectionId: string;
    userName: string;

    constructor(connectionId: string = '', userName: string = '') {
        this.connectionId = connectionId;
        this.userName = userName;
    }
}
