export class ChatMessage {
    message: string;
    date: Date;
    userName: string;
    sender: boolean;

    constructor(message: string = '', date: string = '', username: string = '') {
        this.message = message;
        this.date = new Date(date);
        this.userName = username;
        this.sender = false;
    }
}
