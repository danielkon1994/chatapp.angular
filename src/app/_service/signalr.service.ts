import { ChatMessage } from './../_model/chatMessage.model';
import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Subject } from 'rxjs';
import { UserDetail } from '../_model/userDetail.model';

@Injectable({
  providedIn: 'root'
})
export class SignalrService {
  private hubConnection: HubConnection;
  messageReceived = new Subject<ChatMessage>();
  connectionEstablished = new Subject<boolean>();
  connectionToChat = new Subject<boolean>();
  user = new Subject<UserDetail>();

  constructor() {
    this.createConnection();
    this.registerOnServerEvent();
    this.startConnection();
  }

  private createConnection() {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl('http://localhost:5000/chat')
      .build();
  }

  private startConnection() {
    this.hubConnection
      .start()
      .then(() => {
        console.log('Hub connection started');
        this.connectionEstablished.next(true);
      })
      .catch(err => {
        console.log('Error while establishing connection, retrying...');
        setTimeout(this.startConnection(), 5000);
      });
  }

  sendMessage(toUserName: string, message: string) {
    const date = new Date();
    const chatMessage = new ChatMessage(message, date.toString());
    this.hubConnection.invoke('SendMessageToAll', chatMessage)
      .catch(error => { console.log(error); });
  }

  connect(userName: string) {
    this.hubConnection.invoke('Join', userName)
      .catch(error => { console.log(error); });
  }

  private registerOnServerEvent() {
    this.hubConnection.on('NewUserJoined', (userDetail: UserDetail) => {
      this.user.next(userDetail);
    });

    this.hubConnection.on('Joined', (message: ChatMessage) => {
      this.connectionToChat.next(true);
    });

    this.hubConnection.on('OnlineUser', (message: ChatMessage) => {
      this.messageReceived.next(message);
    });
  }

}
