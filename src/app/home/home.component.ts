import { UserDetail } from './../_model/userDetail.model';
import { SignalrService } from './../_service/signalr.service';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';
import { ChatMessage } from '../_model/chatMessage.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private hubConnection: HubConnection;
  messages: ChatMessage[];
  message: string;
  user: UserDetail;
  users: UserDetail[];
  selectedUserName = '';
  connectedToChat = false;

  constructor() { }

  ngOnInit() {
    this.messages = [];
    this.createConnection();
    this.startConnection();
    this.registerOnServerEvent();
  }

  selectUser(userName: string) {
    this.selectedUserName = userName;
    this.messages = [];
  }

  sendMessage() {
    const date = new Date();
    const chatMessage = new ChatMessage(this.message, date.toString());
    this.hubConnection.invoke('SendMessage', this.selectedUserName, chatMessage)
      .catch(error => { console.log(error); });
    this.message = '';
  }

  private registerOnServerEvent() {
    this.hubConnection.on('NewUserJoined', (userDetail: UserDetail) => {
      this.users.push(userDetail);
    });

    this.hubConnection.on('Joined', (userDetail: UserDetail) => {
      this.user = userDetail;
      this.connectedToChat = true;
    });

    this.hubConnection.on('OnlineUser', (users: UserDetail[]) => {
      this.users = users;
    });

    this.hubConnection.on('SendMessageFrom', (message: ChatMessage, userFrom: UserDetail) => {
      this.messages.push(message);
      this.selectedUserName = userFrom.userName;
    });

    this.hubConnection.on('SendMessageTo', (message: ChatMessage, userTo: UserDetail) => {
      message.sender = true;
      this.messages.push(message);
    });

    this.hubConnection.on('Departed', (userName: string) => {
      if (this.selectedUserName === userName) {
        this.selectedUserName = '';
      }
      this.users = this.users.filter((user: UserDetail) => user.userName !== userName);
    });
  }

  connect() {
    const userName = prompt('Enter your username');
    if (!!userName) {
      this.hubConnection.invoke('Join', userName)
        .catch(error => { console.log(error); });
    }
  }

  disconnect() {
    if (!!this.user) {
      this.connectedToChat = false;
      this.user = null;
      this.users = [];
      this.hubConnection.invoke('Leave', this.user.userName)
        .catch(error => { console.log(error); });
    }
    else
    {
      alert('You are not connected');
    }
  }

  private createConnection() {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl('http://localhost:5000/chat')
      .build();
  }

  private startConnection() {
    this.hubConnection
      .start()
      .then(() => {
        console.log('Hub connection started');
        this.connect();
      })
      .catch(err => {
        console.log('Error while establishing connection, retrying...');
        setTimeout(this.startConnection(), 5000);
      });
  }
}
